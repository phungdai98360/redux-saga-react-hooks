import * as actionType from "./../actions/actionType";
const ticket = (state = [], action) => {
  switch (action.type) {
    case actionType.FET_TICKET_SUCCESS:
      state = action.data;
      return [...state];
    case actionType.FET_TICKET_FAIL:
      return [];
    case actionType.ADD_TICKET:
      return [...state];
    default:
      return [...state];
  }
};
export default ticket;
