import { combineReducers } from "redux";
import ticket from "./ticket";
const reducer = combineReducers({
  ticket,
});
export default reducer;
