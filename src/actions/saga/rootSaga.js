import { all } from "redux-saga/effects";
import {
  watchFetchTicket,
  watchAddTicket,
  watchUpdateTicket,
} from "./actionSaga";
export default function* rootSaga() {
  yield all([watchFetchTicket(), watchAddTicket(), watchUpdateTicket()]);
}
