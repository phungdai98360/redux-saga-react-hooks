import callApi from "./../../util/callApi";
import {
  GETTICKET,
  INSERTTICKET,
  UPDATETICKET,
} from "./../../config/configApi";
function* getTicketFromApi() {
  const res = yield callApi("GET", GETTICKET, null);
  const ticket = res.data;
  return ticket;
}
function* addTicketFromApi(data) {
  let params = {
    mave: data.mave,
    soghe: data.soghe,
    macb: data.chuyenbay,
    gia: data.gia,
  };
  const result = yield callApi("POST", INSERTTICKET, params);
  return result;
}
function* updateTicketFromApi(data) {
  let params = {
    mave: data.mave,
    soghe: data.soghe,
    macb: data.chuyenbay,
    gia: data.gia,
  };
  const result = yield callApi("PUT", UPDATETICKET, params);
  console.log(result);
  return result;
}
export const Api = {
  getTicketFromApi,
  addTicketFromApi,
  updateTicketFromApi,
};
