import * as actionType from "./../actionType";
import { put, takeLatest, takeEvery, call } from "redux-saga/effects";
import { Api } from "./api";
function* fetTicket() {
  try {
    const data = yield Api.getTicketFromApi();
    yield put({
      type: actionType.FET_TICKET_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionType.FET_TICKET_FAIL,
      error,
    });
  }
}
function* addTicket(action) {
  try {
    const result = yield call(Api.addTicketFromApi, action.values);
    console.log(result);
    if (result) {
      alert("Thêm thành công");
    }
    yield put({ type: actionType.FET_TICKET });
  } catch (error) {
    console.log(error);
  }
}
function* updateTicket(action) {
  try {
    const result = yield call(Api.updateTicketFromApi, action.values);
    if (result) {
      alert("Cập nhật thành công");
      yield put({ type: actionType.FET_TICKET });
    }
  } catch (error) {
    alert("Đã có lỗi");
  }
}
export function* watchFetchTicket() {
  yield takeLatest(actionType.FET_TICKET, fetTicket);
}
export function* watchAddTicket() {
  yield takeEvery(actionType.ADD_TICKET, addTicket);
}
export function* watchUpdateTicket() {
  yield takeEvery(actionType.UPDATE_TICKET, updateTicket);
}
