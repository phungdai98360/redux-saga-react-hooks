import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Form, Button, Col, Row, Input } from "antd";
import { connect } from "react-redux";
import * as actionType from "./../../actions/actionType";
const FormTicket = (props) => {
  const [form] = Form.useForm();
  form.setFieldsValue(props.valuesRecord);
  console.log("form", props.valuesRecord);
  const onFinish = (values) => {
    if (props.statusUpdate === false) {
      props.addTicket(values);
    } else {
      props.updateTicket(values);
    }
    //console.log("Success:", values);
  };
  //   useEffect(() => {
  //     form.setFieldsValue(props.valuesRecord);
  //   }, props.valuesRecord);
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <Form
      //ref={form}
      name="basic"
      form={form}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Row>
        <Col xs={6}></Col>
      </Row>
      <Form.Item
        label="Mã vé"
        name="mave"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Số ghế"
        name="soghe"
        rules={[{ required: true, message: "Please input your password!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Mã chuyến bay"
        name="chuyenbay"
        rules={[{ required: true, message: "Please input your password!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Giá"
        name="gia"
        rules={[{ required: true, message: "Please input your password!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Lưu lại
        </Button>
      </Form.Item>
    </Form>
  );
};

FormTicket.propTypes = {};
const mapDispatchToProps = (dispatch) => {
  return {
    addTicket: (data) => {
      dispatch({ type: actionType.ADD_TICKET, values: data });
    },
    updateTicket: (data) => {
      dispatch({ type: actionType.UPDATE_TICKET, values: data });
    },
  };
};
export default connect(null, mapDispatchToProps)(FormTicket);
