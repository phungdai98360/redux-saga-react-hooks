import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Row, Col, Button, Input, Space, Table, Drawer } from "antd";
import Highlighter from "react-highlight-words";
import { SearchOutlined, PlusCircleOutlined } from "@ant-design/icons";
import { connect, useSelector } from "react-redux";
import * as actionType from "./../../actions/actionType";
import FormTicket from "./formTicket";
const ListTicket = (props) => {
  const [searchText, setsearchText] = useState("");
  const [searchedColumn, setsearchedColumn] = useState("");
  const [visiableDraw, setvisiableDraw] = useState(false);
  const [visible, setvisible] = useState(false);
  const [statusUpdate, setStatusUpdate] = useState(false);
  const [valuesRecord, setValuesRecord] = useState({});
  const arrTicket = [];
  const { ticket } = useSelector((state) => ({
    ticket: state.ticket,
  }));
  if (ticket.length > 0) {
    for (let i = 0; i < ticket.length; i++) {
      arrTicket.push({
        mave: ticket[i].mave,
        soghe: ticket[i].soghe,
        gia: ticket[i].gia ? ticket[i].gia : "500000",
        dadat: !ticket[i].dada ? "Trống" : "Đã đặt",
        chuyenbay:
          ticket && ticket.length > 0 && ticket[i].chuyenbay
            ? ticket[i].chuyenbay.macb
            : "no",
      });
    }
  }

  console.log(arrTicket);
  useEffect(() => {
    props.getListTicket();
  }, []);
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        //setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setsearchText(selectedKeys[0]);
    setsearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setsearchText("");
  };
  const onUpdate = (values) => {
    //console.log(values);
    setvisible(true);
    setStatusUpdate(true);
    setValuesRecord(values);
  };
  const columns = [
    {
      title: "Mã vé",
      dataIndex: "mave",
      key: "mave",

      ...getColumnSearchProps("mave"),
    },
    {
      title: "Số ghế",
      dataIndex: "soghe",
      key: "soghe",

      ...getColumnSearchProps("soghe"),
    },
    {
      title: "Giá",
      dataIndex: "gia",
      key: "gia",

      ...getColumnSearchProps("email"),
    },
    {
      title: "Đã đặt",
      dataIndex: "dadat",
      key: "dadat",

      ...getColumnSearchProps("dadat"),
    },
    {
      title: "Chuyến bay",
      dataIndex: "chuyenbay",
      key: "chuyenbay",

      ...getColumnSearchProps("chuyenbay.macb"),
    },
    {
      title: "Hành động",
      key: "action",
      render: (text, record) => (
        <React.Fragment>
          <Button onClick={() => onUpdate(record)}>Sửa</Button>
          <Button>Xóa</Button>
        </React.Fragment>
      ),
    },
  ];
  const onOpenForm = () => {
    setvisible(true);
    setStatusUpdate(false);
    let temp = {
      mave: "",
      soghe: "",
      chuyenbay: "",
      gia: "",
    };
    setValuesRecord(temp);
  };
  const onClose = () => {
    setvisible(false);
  };
  return (
    <div>
      <Button type="primary" onClick={onOpenForm} icon={<PlusCircleOutlined />}>
        Thêm
      </Button>
      <Row>
        <Col xs={24}>
          <Table columns={columns} dataSource={arrTicket} />
        </Col>
      </Row>
      <Row>
        <Col>
          <Drawer
            title="Thêm vé"
            width={720}
            onClose={onClose}
            visible={visible}
            bodyStyle={{ paddingBottom: 80 }}
          >
            <FormTicket
              valuesRecord={valuesRecord}
              statusUpdate={statusUpdate}
            />
          </Drawer>
        </Col>
      </Row>
    </div>
  );
};

ListTicket.propTypes = {};
const mapDispatchToProps = (dispatch) => {
  return {
    getListTicket: () => {
      dispatch({ type: actionType.FET_TICKET });
    },
  };
};
export default connect(null, mapDispatchToProps)(ListTicket);
